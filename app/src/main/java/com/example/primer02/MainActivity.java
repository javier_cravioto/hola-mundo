package com.example.primer02;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView lblSaludo;
    private EditText txtSaludo2;
    private Button btnPulsame;
    private Button btnLimpiar;
    private Button btnCerrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lblSaludo=(TextView)findViewById(R.id.lblSaludo) ;
        txtSaludo2=(EditText)findViewById(R.id.txtSaludo2) ;
        btnPulsame= (Button)findViewById(R.id.btnSaludo);
        btnLimpiar= (Button)findViewById(R.id.btnLimpiar);
        btnCerrar= (Button)findViewById(R.id.btnCerrar);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblSaludo.setText("");
                txtSaludo2.setText("");


            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnPulsame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtSaludo2.getText().toString().matches("")) {

                    Toast.makeText(MainActivity.this, "Faltó capturar nombre", Toast.LENGTH_SHORT).show();
                } else {

                    String txtSaludar = txtSaludo2.getText().toString();
                    lblSaludo.setText("Hola " + txtSaludo2.getText().toString()+ ", ¿Cómo estás? :)");

                    }
                }
            });
        }

     }

